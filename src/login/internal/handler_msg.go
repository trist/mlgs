package internal

import (
	"mlgs/src/base"
	"reflect"

	"github.com/trist725/myleaf/log"

	"mlgs/src/game"
	"mlgs/src/model"
	"mlgs/src/msg"
	s "mlgs/src/session"
)

func init() {
	regiserMsgHandle(&msg.C2S_Login{}, handleLoginAuth)
	regiserMsgHandle(&msg.C2S_GetServerList{}, handleGetServerList)
}

func regiserMsgHandle(m interface{}, h interface{}) {
	skeleton.RegisterChanRPC(reflect.TypeOf(m), h)
}

func handleLoginAuth(args []interface{}) {
	recv := args[0].(*msg.C2S_Login)
	sender := args[1].(*base.Agent)
	clientID := args[2].(int32)
	send := msg.Get_S2C_Login()

	dbSession := model.SC.GetSession()
	defer model.SC.PutSession(dbSession)

	defer func() {
		sender.WriteMsgWithInt32Prefix(clientID, send)
	}()

	//todo:主动断开客户端
	//todo:已在线踢人

	if s.Mgr().CheckSession(clientID) {
		send.Result = msg.S2C_Login_E_Result_AlreadyLogin
		return
	}
	//if recv.LoginType == msg.C2S_Login_E_LoginType_Guest {
	if account, err := checkAccountExist(dbSession, recv.UID); err == nil && account != nil {
		if user, err := checkUserExist(dbSession, account.ID); err == nil && user != nil {
			send.Result = msg.S2C_Login_E_Result_LoginSuccess
			send.UserID = user.ID
			game.ChanRPC.Go("NewSession", sender, clientID, account, user)
			return
		} else {
			send.Result = msg.S2C_Login_E_Result_Unknown
			log.Error("accout: [%v] exist but user not exist", account)
			return
		}
	}
	//}

	//创建新账号
	newAccount, err := createAccount(dbSession, recv)
	if err != nil {
		send.Result = msg.S2C_Login_E_Result_Unknown
		log.Error("create account failed: [%s]", err.Error())
		return
	}
	defer model.Put_Account(newAccount)
	//创建新用户
	newUser, err := createUser(dbSession, newAccount.ID, recv)
	if err != nil {
		send.Result = msg.S2C_Login_E_Result_Unknown
		log.Error("create user failed: [%s]", err.Error())
		return
	}
	defer model.Put_User(newUser)

	send.UserID = newUser.ID
	game.ChanRPC.Go("NewSession", sender, clientID, newAccount, newUser)
	send.Result = msg.S2C_Login_E_Result_NewAccount
}

func handleGetServerList(args []interface{}) {
	//recv := args[0].(*msg.C2S_GetServerList)
	sender := args[1].(*base.Agent)
	clientID := args[2].(int32)
	send := msg.Get_S2C_GetServerList()

	defer sender.WriteMsgWithInt32Prefix(clientID, send)

	status := msg.Get_ServerStatus()
	status.SerialNum = 1
	status.Name = "我是看戏服"
	status.Stat = msg.ServerStatus_E_Maintain
	send.Servers = append(send.Servers, status)

	status2 := msg.Get_ServerStatus()
	status2.SerialNum = 2
	status2.Name = "我是测试服"
	status2.RoleExist = true
	status2.Stat = msg.ServerStatus_E_Idle
	send.Servers = append(send.Servers, status2)

	status3 := msg.Get_ServerStatus()
	status3.SerialNum = 3
	status3.Name = "我是人气服"
	status3.RoleExist = true
	status3.Stat = msg.ServerStatus_E_Full
	send.Servers = append(send.Servers, status3)

}
