package internal

import (
	"fmt"
	"mlgs/src/conf"
	"mlgs/src/model"
	"mlgs/src/msg"

	"github.com/globalsign/mgo/bson"
	"github.com/trist725/mgsu/db/mongodb"
)

func createUser(dbSession *mongodb.Session, accountId int64, recv *msg.C2S_Login) (*model.User, error) {
	//user关联了accountID
	newUser, err := model.SC.CreateUserByMsg(accountId, recv)
	if err != nil {
		return nil, fmt.Errorf("create user fail, %s", err)

	}
	if err := newUser.Insert(dbSession, conf.Server.DBName); err != nil {
		return nil, fmt.Errorf("insert new user[%#v] fail, %s", newUser, err)
	}
	return newUser, nil
}

func checkUserExist(dbSession *mongodb.Session, accountId int64) (*model.User, error) {
	user, err := model.SC.FindOne_User(
		dbSession, bson.M{
			"accountid": accountId,
		},
	)

	return user, err
}
