package internal

import (
	"mlgs/src/model"
	"mlgs/src/session"
	"time"

	"github.com/trist725/myleaf/gate"
	"github.com/trist725/myleaf/log"
)

func init() {
	skeleton.RegisterChanRPC("NewAgent", rpcNewAgent)
	skeleton.RegisterChanRPC("NewSession", rpcHandleNewSession)
}

func rpcNewAgent(args []interface{}) {
	a := args[0].(gate.Agent)
	_ = a
}

const saveIntervalMinute = 1 // 保存数据间隔（单位：分钟）

func rpcHandleNewSession(args []interface{}) {
	a := args[0].(gate.Agent)
	cid := args[1].(int32)
	account := args[2].(*model.Account)
	user := args[3].(*model.User)

	newSession := session.New(a, cid, account, user, nil)
	log.Debug("[%s] login success", newSession.Sign())
	log.Debug("current session count: %d", session.Mgr().Count())

	//定时写库
	var f func()
	f = func() {
		timer := skeleton.AfterFunc(saveIntervalMinute*time.Minute, func() {
			f()
			//实际要做的事
			newSession.SaveData()
		})
		newSession.SetTimer(timer)
	}
	f()
}
