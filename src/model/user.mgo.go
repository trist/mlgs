// Code generated by protoc-gen-mgo-go. DO NOT EDIT IT!!!
// source: user.proto

package model

import (
	json "encoding/json"
	fmt "fmt"
	mgo "github.com/globalsign/mgo"
	_ "github.com/gogo/protobuf/gogoproto"
	proto "github.com/gogo/protobuf/proto"
	mongodb "github.com/trist725/mgsu/db/mongodb"
	math "math"
	msg "mlgs/src/msg"
	sync "sync"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

var _ = json.Marshal
var _ = msg.PH

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// collection [Attr] begin

func New_Attr() *Attr {
	m := &Attr{}
	return m
}

func (m Attr) JsonString() string {
	ba, _ := json.Marshal(m)
	return "Attr:" + string(ba)
}

func (m *Attr) ResetEx() {

	m.Hp = 0

	m.External_Atk = 0

	m.Internal_Atk = 0

	m.External_Def = 0

	m.Internal_Def = 0

	m.Penetration = 0

	m.Hit_Prob = 0

	m.Dodge = 0

	m.Crit_Atk = 0

	m.Crit_Def = 0

}

func (m Attr) Clone() *Attr {
	n, ok := g_Attr_Pool.Get().(*Attr)
	if !ok || n == nil {
		n = &Attr{}
	}

	n.Hp = m.Hp

	n.External_Atk = m.External_Atk

	n.Internal_Atk = m.Internal_Atk

	n.External_Def = m.External_Def

	n.Internal_Def = m.Internal_Def

	n.Penetration = m.Penetration

	n.Hit_Prob = m.Hit_Prob

	n.Dodge = m.Dodge

	n.Crit_Atk = m.Crit_Atk

	n.Crit_Def = m.Crit_Def

	return n
}

func Clone_Attr_Slice(dst []*Attr, src []*Attr) []*Attr {
	for _, i := range dst {
		Put_Attr(i)
	}
	if len(src) > 0 {
		dst = make([]*Attr, len(src))
		for i, e := range src {
			if e != nil {
				dst[i] = e.Clone()
			}
		}
	} else {
		//dst = []*Attr{}
		dst = nil
	}
	return dst
}

func (m Attr) ToMsg(n *msg.Attr) *msg.Attr {
	if n == nil {
		n = msg.Get_Attr()
	}

	n.Hp = m.Hp

	n.External_Atk = m.External_Atk

	n.Internal_Atk = m.Internal_Atk

	n.External_Def = m.External_Def

	n.Internal_Def = m.Internal_Def

	n.Penetration = m.Penetration

	n.Hit_Prob = m.Hit_Prob

	n.Dodge = m.Dodge

	n.Crit_Atk = m.Crit_Atk

	n.Crit_Def = m.Crit_Def

	return n
}

var g_Attr_Pool = sync.Pool{}

func Get_Attr() *Attr {
	m, ok := g_Attr_Pool.Get().(*Attr)
	if !ok {
		m = New_Attr()
	} else {
		if m == nil {
			m = New_Attr()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_Attr(i interface{}) {
	if m, ok := i.(*Attr); ok && m != nil {
		g_Attr_Pool.Put(i)
	}
}

// collection [Attr] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// collection [ExAttr] begin

func New_ExAttr() *ExAttr {
	m := &ExAttr{}
	return m
}

func (m ExAttr) JsonString() string {
	ba, _ := json.Marshal(m)
	return "ExAttr:" + string(ba)
}

func (m *ExAttr) ResetEx() {

	m.Move_Speed = 0

	m.Atk_Speed = 0

	m.DamageAmplif = 0

	m.DamageReduce = 0

	m.Palsy_Atk = 0

	m.Palsy_Def = 0

	m.Crit_Damage = 0

	m.Crit_Prob = 0

	m.Acc_Prob = 0

}

func (m ExAttr) Clone() *ExAttr {
	n, ok := g_ExAttr_Pool.Get().(*ExAttr)
	if !ok || n == nil {
		n = &ExAttr{}
	}

	n.Move_Speed = m.Move_Speed

	n.Atk_Speed = m.Atk_Speed

	n.DamageAmplif = m.DamageAmplif

	n.DamageReduce = m.DamageReduce

	n.Palsy_Atk = m.Palsy_Atk

	n.Palsy_Def = m.Palsy_Def

	n.Crit_Damage = m.Crit_Damage

	n.Crit_Prob = m.Crit_Prob

	n.Acc_Prob = m.Acc_Prob

	return n
}

func Clone_ExAttr_Slice(dst []*ExAttr, src []*ExAttr) []*ExAttr {
	for _, i := range dst {
		Put_ExAttr(i)
	}
	if len(src) > 0 {
		dst = make([]*ExAttr, len(src))
		for i, e := range src {
			if e != nil {
				dst[i] = e.Clone()
			}
		}
	} else {
		//dst = []*ExAttr{}
		dst = nil
	}
	return dst
}

func (m ExAttr) ToMsg(n *msg.ExAttr) *msg.ExAttr {
	if n == nil {
		n = msg.Get_ExAttr()
	}

	n.Move_Speed = m.Move_Speed

	n.Atk_Speed = m.Atk_Speed

	n.DamageAmplif = m.DamageAmplif

	n.DamageReduce = m.DamageReduce

	n.Palsy_Atk = m.Palsy_Atk

	n.Palsy_Def = m.Palsy_Def

	n.Crit_Damage = m.Crit_Damage

	n.Crit_Prob = m.Crit_Prob

	n.Acc_Prob = m.Acc_Prob

	return n
}

var g_ExAttr_Pool = sync.Pool{}

func Get_ExAttr() *ExAttr {
	m, ok := g_ExAttr_Pool.Get().(*ExAttr)
	if !ok {
		m = New_ExAttr()
	} else {
		if m == nil {
			m = New_ExAttr()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_ExAttr(i interface{}) {
	if m, ok := i.(*ExAttr); ok && m != nil {
		g_ExAttr_Pool.Put(i)
	}
}

// collection [ExAttr] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// collection [Role] begin

func New_Role() *Role {
	m := &Role{

		Attr: Get_Attr(),

		ExAttr: Get_ExAttr(),
	}
	return m
}

func (m Role) JsonString() string {
	ba, _ := json.Marshal(m)
	return "Role:" + string(ba)
}

func (m *Role) ResetEx() {

	m.ID = 0

	m.Level = 0

	m.NickName = ""

	for _, i := range m.Items {
		Put_Item(i)
	}

	//m.Items = []*Item{}
	m.Items = nil

	for _, i := range m.Monies {
		Put_Money(i)
	}

	//m.Monies = []*Money{}
	m.Monies = nil

	m.Sex = ""

	m.CreateTime = 0

	m.LastLoginTime = 0

	m.LastLogoutTime = 0

	m.Exp = 0

	if m.Attr != nil {
		m.Attr.ResetEx()
	} else {
		m.Attr = Get_Attr()
	}

	if m.ExAttr != nil {
		m.ExAttr.ResetEx()
	} else {
		m.ExAttr = Get_ExAttr()
	}

}

func (m Role) Clone() *Role {
	n, ok := g_Role_Pool.Get().(*Role)
	if !ok || n == nil {
		n = &Role{}
	}

	n.ID = m.ID

	n.Level = m.Level

	n.NickName = m.NickName

	if len(m.Items) > 0 {
		n.Items = make([]*Item, len(m.Items))
		for i, e := range m.Items {

			if e != nil {
				n.Items[i] = e.Clone()
			}

		}
	} else {
		//n.Items = []*Item{}
		n.Items = nil
	}

	if len(m.Monies) > 0 {
		n.Monies = make([]*Money, len(m.Monies))
		for i, e := range m.Monies {

			if e != nil {
				n.Monies[i] = e.Clone()
			}

		}
	} else {
		//n.Monies = []*Money{}
		n.Monies = nil
	}

	n.Sex = m.Sex

	n.CreateTime = m.CreateTime

	n.LastLoginTime = m.LastLoginTime

	n.LastLogoutTime = m.LastLogoutTime

	n.Exp = m.Exp

	if m.Attr != nil {
		n.Attr = m.Attr.Clone()
	}

	if m.ExAttr != nil {
		n.ExAttr = m.ExAttr.Clone()
	}

	return n
}

func Clone_Role_Slice(dst []*Role, src []*Role) []*Role {
	for _, i := range dst {
		Put_Role(i)
	}
	if len(src) > 0 {
		dst = make([]*Role, len(src))
		for i, e := range src {
			if e != nil {
				dst[i] = e.Clone()
			}
		}
	} else {
		//dst = []*Role{}
		dst = nil
	}
	return dst
}

func (m Role) ToMsg(n *msg.Role) *msg.Role {
	if n == nil {
		n = msg.Get_Role()
	}

	n.ID = m.ID

	n.Level = m.Level

	n.NickName = m.NickName

	if len(m.Items) > 0 {

		n.Items = make([]*msg.Item, len(m.Items))
		for i, e := range m.Items {
			if e != nil {
				n.Items[i] = e.ToMsg(n.Items[i])
			} else {
				n.Items[i] = msg.Get_Item()
			}
		}

	} else {
		//n.Items = []*Item{}
		n.Items = nil
	}

	if len(m.Monies) > 0 {

		n.Monies = make([]*msg.Money, len(m.Monies))
		for i, e := range m.Monies {
			if e != nil {
				n.Monies[i] = e.ToMsg(n.Monies[i])
			} else {
				n.Monies[i] = msg.Get_Money()
			}
		}

	} else {
		//n.Monies = []*Money{}
		n.Monies = nil
	}

	n.Sex = m.Sex

	n.Exp = m.Exp

	if m.Attr != nil {
		n.Attr = m.Attr.ToMsg(n.Attr)
	}

	if m.ExAttr != nil {
		n.ExAttr = m.ExAttr.ToMsg(n.ExAttr)
	}

	return n
}

var g_Role_Pool = sync.Pool{}

func Get_Role() *Role {
	m, ok := g_Role_Pool.Get().(*Role)
	if !ok {
		m = New_Role()
	} else {
		if m == nil {
			m = New_Role()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_Role(i interface{}) {
	if m, ok := i.(*Role); ok && m != nil {
		g_Role_Pool.Put(i)
	}
}

// collection [Role] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// collection [User] begin

func New_User() *User {
	m := &User{}
	return m
}

func (m User) JsonString() string {
	ba, _ := json.Marshal(m)
	return "User:" + string(ba)
}

func (m *User) ResetEx() {

	m.ID = 0

	m.AccountID = 0

	m.CreateTime = 0

	m.LastLoginTime = 0

	m.LastLogoutTime = 0

	for _, i := range m.Roles {
		Put_Role(i)
	}

	//m.Roles = []*Role{}
	m.Roles = nil

}

func (m User) Clone() *User {
	n, ok := g_User_Pool.Get().(*User)
	if !ok || n == nil {
		n = &User{}
	}

	n.ID = m.ID

	n.AccountID = m.AccountID

	n.CreateTime = m.CreateTime

	n.LastLoginTime = m.LastLoginTime

	n.LastLogoutTime = m.LastLogoutTime

	if len(m.Roles) > 0 {
		n.Roles = make([]*Role, len(m.Roles))
		for i, e := range m.Roles {

			if e != nil {
				n.Roles[i] = e.Clone()
			}

		}
	} else {
		//n.Roles = []*Role{}
		n.Roles = nil
	}

	return n
}

func Clone_User_Slice(dst []*User, src []*User) []*User {
	for _, i := range dst {
		Put_User(i)
	}
	if len(src) > 0 {
		dst = make([]*User, len(src))
		for i, e := range src {
			if e != nil {
				dst[i] = e.Clone()
			}
		}
	} else {
		//dst = []*User{}
		dst = nil
	}
	return dst
}

func (sc SimpleClient) FindOne_User(session *mongodb.Session, query interface{}) (one *User, err error) {
	one = Get_User()
	err = session.DB(sc.dbName).C(TblUser).Find(query).One(one)
	if err != nil {
		Put_User(one)
		return nil, err
	}
	return
}

func (sc SimpleClient) FindSome_User(session *mongodb.Session, query interface{}) (some []*User, err error) {
	some = []*User{}
	err = session.DB(sc.dbName).C(TblUser).Find(query).All(&some)
	if err != nil {
		return nil, err
	}
	return
}

func (sc SimpleClient) UpdateSome_User(session *mongodb.Session, selector interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	info, err = session.DB(sc.dbName).C(TblUser).UpdateAll(selector, update)
	return
}

func (sc SimpleClient) Upsert_User(session *mongodb.Session, selector interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	info, err = session.DB(sc.dbName).C(TblUser).Upsert(selector, update)
	return
}

func (sc SimpleClient) UpsertID_User(session *mongodb.Session, id interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	info, err = session.DB(sc.dbName).C(TblUser).UpsertId(id, update)
	return
}

func (m User) Insert(session *mongodb.Session, dbName string) error {
	return session.DB(dbName).C(TblUser).Insert(m)
}

func (m User) Update(session *mongodb.Session, dbName string, selector interface{}, update interface{}) error {
	return session.DB(dbName).C(TblUser).Update(selector, update)
}

func (m User) UpdateByID(session *mongodb.Session, dbName string) error {
	return session.DB(dbName).C(TblUser).UpdateId(m.ID, m)
}

func (m User) RemoveByID(session *mongodb.Session, dbName string) error {
	return session.DB(dbName).C(TblUser).RemoveId(m.ID)
}

func (m User) ToMsg(n *msg.User) *msg.User {
	if n == nil {
		n = msg.Get_User()
	}

	n.ID = m.ID

	if len(m.Roles) > 0 {

		n.Roles = make([]*msg.Role, len(m.Roles))
		for i, e := range m.Roles {
			if e != nil {
				n.Roles[i] = e.ToMsg(n.Roles[i])
			} else {
				n.Roles[i] = msg.Get_Role()
			}
		}

	} else {
		//n.Roles = []*Role{}
		n.Roles = nil
	}

	return n
}

var g_User_Pool = sync.Pool{}

func Get_User() *User {
	m, ok := g_User_Pool.Get().(*User)
	if !ok {
		m = New_User()
	} else {
		if m == nil {
			m = New_User()
		} else {
			m.ResetEx()
		}
	}
	return m
}

func Put_User(i interface{}) {
	if m, ok := i.(*User); ok && m != nil {
		g_User_Pool.Put(i)
	}
}

// collection [User] end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
