package gate

import (
	"mlgs/src/gate/internal"
)

var (
	Module  = new(internal.Module)
	ChanRPC = internal.ChanRPC
	Gates   = internal.Gates
)
