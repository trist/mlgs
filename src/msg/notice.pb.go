// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: notice.proto

package msg

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

type Notice struct {
	///公告类型,1-精彩公告，2-精彩活动
	Type int32 `protobuf:"varint,1,opt,name=Type,proto3" json:"Type,omitempty"`
	///公告名称
	Name string `protobuf:"bytes,2,opt,name=Name,proto3" json:"Name,omitempty"`
	///正文标题
	Title string `protobuf:"bytes,3,opt,name=Title,proto3" json:"Title,omitempty"`
	///内容
	Content string `protobuf:"bytes,4,opt,name=Content,proto3" json:"Content,omitempty"`
}

func (m *Notice) Reset()         { *m = Notice{} }
func (m *Notice) String() string { return proto.CompactTextString(m) }
func (*Notice) ProtoMessage()    {}
func (*Notice) Descriptor() ([]byte, []int) {
	return fileDescriptor_642492014393dbdb, []int{0}
}
func (m *Notice) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *Notice) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_Notice.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *Notice) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Notice.Merge(m, src)
}
func (m *Notice) XXX_Size() int {
	return m.Size()
}
func (m *Notice) XXX_DiscardUnknown() {
	xxx_messageInfo_Notice.DiscardUnknown(m)
}

var xxx_messageInfo_Notice proto.InternalMessageInfo

func (m *Notice) GetType() int32 {
	if m != nil {
		return m.Type
	}
	return 0
}

func (m *Notice) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Notice) GetTitle() string {
	if m != nil {
		return m.Title
	}
	return ""
}

func (m *Notice) GetContent() string {
	if m != nil {
		return m.Content
	}
	return ""
}

type C2S_GetNotices struct {
}

func (m *C2S_GetNotices) Reset()         { *m = C2S_GetNotices{} }
func (m *C2S_GetNotices) String() string { return proto.CompactTextString(m) }
func (*C2S_GetNotices) ProtoMessage()    {}
func (*C2S_GetNotices) Descriptor() ([]byte, []int) {
	return fileDescriptor_642492014393dbdb, []int{1}
}
func (m *C2S_GetNotices) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *C2S_GetNotices) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_C2S_GetNotices.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *C2S_GetNotices) XXX_Merge(src proto.Message) {
	xxx_messageInfo_C2S_GetNotices.Merge(m, src)
}
func (m *C2S_GetNotices) XXX_Size() int {
	return m.Size()
}
func (m *C2S_GetNotices) XXX_DiscardUnknown() {
	xxx_messageInfo_C2S_GetNotices.DiscardUnknown(m)
}

var xxx_messageInfo_C2S_GetNotices proto.InternalMessageInfo

type S2C_GetNotices struct {
	Notices []*Notice `protobuf:"bytes,1,rep,name=Notices,proto3" json:"Notices,omitempty"`
}

func (m *S2C_GetNotices) Reset()         { *m = S2C_GetNotices{} }
func (m *S2C_GetNotices) String() string { return proto.CompactTextString(m) }
func (*S2C_GetNotices) ProtoMessage()    {}
func (*S2C_GetNotices) Descriptor() ([]byte, []int) {
	return fileDescriptor_642492014393dbdb, []int{2}
}
func (m *S2C_GetNotices) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *S2C_GetNotices) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_S2C_GetNotices.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *S2C_GetNotices) XXX_Merge(src proto.Message) {
	xxx_messageInfo_S2C_GetNotices.Merge(m, src)
}
func (m *S2C_GetNotices) XXX_Size() int {
	return m.Size()
}
func (m *S2C_GetNotices) XXX_DiscardUnknown() {
	xxx_messageInfo_S2C_GetNotices.DiscardUnknown(m)
}

var xxx_messageInfo_S2C_GetNotices proto.InternalMessageInfo

func (m *S2C_GetNotices) GetNotices() []*Notice {
	if m != nil {
		return m.Notices
	}
	return nil
}

func init() {
	proto.RegisterType((*Notice)(nil), "msg.Notice")
	proto.RegisterType((*C2S_GetNotices)(nil), "msg.C2S_GetNotices")
	proto.RegisterType((*S2C_GetNotices)(nil), "msg.S2C_GetNotices")
}

func init() { proto.RegisterFile("notice.proto", fileDescriptor_642492014393dbdb) }

var fileDescriptor_642492014393dbdb = []byte{
	// 192 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0xc9, 0xcb, 0x2f, 0xc9,
	0x4c, 0x4e, 0xd5, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0xce, 0x2d, 0x4e, 0x57, 0x4a, 0xe0,
	0x62, 0xf3, 0x03, 0x0b, 0x0a, 0x09, 0x71, 0xb1, 0x84, 0x54, 0x16, 0xa4, 0x4a, 0x30, 0x2a, 0x30,
	0x6a, 0xb0, 0x06, 0x81, 0xd9, 0x20, 0x31, 0xbf, 0xc4, 0xdc, 0x54, 0x09, 0x26, 0x05, 0x46, 0x0d,
	0xce, 0x20, 0x30, 0x5b, 0x48, 0x84, 0x8b, 0x35, 0x24, 0xb3, 0x24, 0x27, 0x55, 0x82, 0x19, 0x2c,
	0x08, 0xe1, 0x08, 0x49, 0x70, 0xb1, 0x3b, 0xe7, 0xe7, 0x95, 0xa4, 0xe6, 0x95, 0x48, 0xb0, 0x80,
	0xc5, 0x61, 0x5c, 0x25, 0x01, 0x2e, 0x3e, 0x67, 0xa3, 0xe0, 0x78, 0xf7, 0xd4, 0x12, 0x88, 0x45,
	0xc5, 0x4a, 0xe6, 0x5c, 0x7c, 0xc1, 0x46, 0xce, 0x48, 0x22, 0x42, 0xaa, 0x5c, 0xec, 0x50, 0xa6,
	0x04, 0xa3, 0x02, 0xb3, 0x06, 0xb7, 0x11, 0xb7, 0x5e, 0x6e, 0x71, 0xba, 0x1e, 0x44, 0x2c, 0x08,
	0x26, 0xe7, 0x24, 0x71, 0xe2, 0x91, 0x1c, 0xe3, 0x85, 0x47, 0x72, 0x8c, 0x0f, 0x1e, 0xc9, 0x31,
	0x4e, 0x78, 0x2c, 0xc7, 0x70, 0xe1, 0xb1, 0x1c, 0xc3, 0x8d, 0xc7, 0x72, 0x0c, 0x49, 0x6c, 0x60,
	0x2f, 0x19, 0x03, 0x02, 0x00, 0x00, 0xff, 0xff, 0x43, 0x4a, 0xbf, 0x98, 0xe2, 0x00, 0x00, 0x00,
}

func (m *Notice) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *Notice) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *Notice) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if len(m.Content) > 0 {
		i -= len(m.Content)
		copy(dAtA[i:], m.Content)
		i = encodeVarintNotice(dAtA, i, uint64(len(m.Content)))
		i--
		dAtA[i] = 0x22
	}
	if len(m.Title) > 0 {
		i -= len(m.Title)
		copy(dAtA[i:], m.Title)
		i = encodeVarintNotice(dAtA, i, uint64(len(m.Title)))
		i--
		dAtA[i] = 0x1a
	}
	if len(m.Name) > 0 {
		i -= len(m.Name)
		copy(dAtA[i:], m.Name)
		i = encodeVarintNotice(dAtA, i, uint64(len(m.Name)))
		i--
		dAtA[i] = 0x12
	}
	if m.Type != 0 {
		i = encodeVarintNotice(dAtA, i, uint64(m.Type))
		i--
		dAtA[i] = 0x8
	}
	return len(dAtA) - i, nil
}

func (m *C2S_GetNotices) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *C2S_GetNotices) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *C2S_GetNotices) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	return len(dAtA) - i, nil
}

func (m *S2C_GetNotices) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *S2C_GetNotices) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *S2C_GetNotices) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if len(m.Notices) > 0 {
		for iNdEx := len(m.Notices) - 1; iNdEx >= 0; iNdEx-- {
			{
				size, err := m.Notices[iNdEx].MarshalToSizedBuffer(dAtA[:i])
				if err != nil {
					return 0, err
				}
				i -= size
				i = encodeVarintNotice(dAtA, i, uint64(size))
			}
			i--
			dAtA[i] = 0xa
		}
	}
	return len(dAtA) - i, nil
}

func encodeVarintNotice(dAtA []byte, offset int, v uint64) int {
	offset -= sovNotice(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *Notice) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if m.Type != 0 {
		n += 1 + sovNotice(uint64(m.Type))
	}
	l = len(m.Name)
	if l > 0 {
		n += 1 + l + sovNotice(uint64(l))
	}
	l = len(m.Title)
	if l > 0 {
		n += 1 + l + sovNotice(uint64(l))
	}
	l = len(m.Content)
	if l > 0 {
		n += 1 + l + sovNotice(uint64(l))
	}
	return n
}

func (m *C2S_GetNotices) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	return n
}

func (m *S2C_GetNotices) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if len(m.Notices) > 0 {
		for _, e := range m.Notices {
			l = e.Size()
			n += 1 + l + sovNotice(uint64(l))
		}
	}
	return n
}

func sovNotice(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozNotice(x uint64) (n int) {
	return sovNotice(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *Notice) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowNotice
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: Notice: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: Notice: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field Type", wireType)
			}
			m.Type = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowNotice
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.Type |= int32(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Name", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowNotice
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthNotice
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthNotice
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Name = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 3:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Title", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowNotice
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthNotice
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthNotice
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Title = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 4:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Content", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowNotice
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthNotice
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthNotice
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Content = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipNotice(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthNotice
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthNotice
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *C2S_GetNotices) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowNotice
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: C2S_GetNotices: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: C2S_GetNotices: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		default:
			iNdEx = preIndex
			skippy, err := skipNotice(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthNotice
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthNotice
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *S2C_GetNotices) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowNotice
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: S2C_GetNotices: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: S2C_GetNotices: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Notices", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowNotice
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthNotice
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthNotice
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Notices = append(m.Notices, &Notice{})
			if err := m.Notices[len(m.Notices)-1].Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipNotice(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthNotice
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthNotice
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipNotice(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowNotice
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowNotice
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowNotice
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthNotice
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupNotice
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthNotice
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthNotice        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowNotice          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupNotice = fmt.Errorf("proto: unexpected end of group")
)
