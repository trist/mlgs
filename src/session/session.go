package session

import (
	"fmt"
	"mlgs/src/base"
	"mlgs/src/conf"
	"mlgs/src/model"
	"sync/atomic"
	"time"

	"github.com/trist725/myleaf/gate"
	"github.com/trist725/myleaf/log"
	"github.com/trist725/myleaf/timer"
)

//非线程安全
type Session struct {
	id uint64
	//定时写库
	timer *timer.Timer
	sign  string // 日志标识

	agent     gate.Agent
	closeFlag int32
	user      *model.User    // 需要保存到数据库的用户数据
	account   *model.Account // 帐号数据
	role      *model.Role    //当前角色数据
	//cache          *cache.Player  // 玩家游戏过程中的数据
	lastActiveTime int64 //上次活动时间
	clientID       int32
}

var gSessionId uint64

func New(agent gate.Agent, clientID int32, account *model.Account, user *model.User, role *model.Role) *Session {
	session := &Session{
		agent:    agent,
		account:  account,
		user:     user,
		role:     role,
		id:       atomic.AddUint64(&gSessionId, 1),
		clientID: clientID,
	}

	if role != nil {
		session.SetSign(fmt.Sprintf("user-%d-role-%d-%s", user.ID, role.ID, role.NickName))
	}

	if gSessionManager == nil {
		panic("new session failed, because gSessionManager is nil")
	}
	gSessionManager.putSession(session)
	return session
}

func (s *Session) ID() uint64 {
	return s.id
}

func (s *Session) AccountData() *model.Account {
	return s.account
}

func (s *Session) UserData() *model.User {
	return s.user
}

func (s *Session) SetAccountData(account *model.Account) {
	s.account = account
}

func (s *Session) SetUserData(user *model.User) {
	s.user = user
}

func (s *Session) CurRoleData() *model.Role {
	return s.role
}

func (s *Session) SetCurRoleData(role *model.Role) {
	s.role = role
}

func (s *Session) SaveData() {
	if s.user != nil {
		// 保存用户数据
		log.Debug("[%s] save data on [%v]", s.sign, time.Now())
		dbSession := model.SC.GetSession()
		if err := s.user.UpdateByID(dbSession, conf.Server.DBName); err != nil {
			log.Error("[%s], save data error:[%s]", s.sign, err)
		}
		model.SC.PutSession(dbSession)
	}
}

func (session *Session) IsClosed() bool {
	return atomic.LoadInt32(&session.closeFlag) == 1
}

//todo：断线重连,deepcopy保存快照
func (s *Session) Close() error {
	if atomic.CompareAndSwapInt32(&s.closeFlag, 0, 1) {
		if gSessionManager == nil {
			panic("close session failed because gSessionManager is nil")
		}
		//更新最后登出时间
		if s.user != nil {
			s.user.LastLogoutTime = time.Now().Unix()
		}
		s.SaveData()
		if s.timer != nil {
			s.timer.Stop()
		}
		//if s.cache != nil {
		//	//游戏中,不删session
		//
		//}
		s.agent.(*base.Agent).WriteCmd(0, s.clientID)
		gSessionManager.delSession(s)
	}
	return nil
}

func (s *Session) Sign() string {
	return s.sign
}

func (s *Session) SetSign(sign string) {
	s.sign = sign
}

func (s *Session) SetTimer(t *timer.Timer) {
	s.timer = t
}

//func (s *Session) Player() *cache.Player {
//	return s.cache
//}
//
//func (s *Session) SetPlayer(p *cache.Player) {
//	s.cache = p
//}

func (s *Session) Agent() gate.Agent {
	return s.agent
}

func (s *Session) Update() {
	s.lastActiveTime = time.Now().Unix()
}

func (s *Session) SetAgent(a gate.Agent) {
	s.agent = a
}

func (s *Session) SetCloseFlag(f int32) {
	atomic.StoreInt32(&s.closeFlag, f)
}
