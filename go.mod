module mlgs

go 1.14

require (
	github.com/astaxie/beego v1.12.2 // indirect
	github.com/frankban/quicktest v1.10.0 // indirect
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/gogo/protobuf v1.3.1
	github.com/kr/text v0.2.0 // indirect
	github.com/tealeg/xlsx v1.0.6-0.20191105032217-48f552b38ead
	github.com/trist725/mgsu/db v0.0.0-20200624064338-e35efa53c32e
	github.com/trist725/mgsu/log v0.0.0-20200624064338-e35efa53c32e // indirect
	github.com/trist725/mgsu/util v0.0.0-20200624064338-e35efa53c32e
	github.com/trist725/myleaf v0.0.0-20200617034648-65d050991516
	google.golang.org/protobuf v1.25.0 // indirect
)
